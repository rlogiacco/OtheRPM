## Improve or contribute

Both the schematics and PCB files are freely available for modification on the _CircuitMaker_ community:
*  [through hole for Othermill Pro / BantamTools Desktop Mill](https://workspace.circuitmaker.com/Projects/Details/Roberto-Lo-Giacco/OtheRPM), available as single or double layer, for common anode or common cathode display
*  [through hole for 3020 CNC](https://circuitmaker.com/Projects/Details/Roberto-Lo-Giacco/CNC-RPM)
*  [surface mount for Othermill Pro / BantamTools Desktop Mill](https://workspace.circuitmaker.com/Projects/Details/Roberto-Lo-Giacco/OtheRPM-SMD), available as single or double layer, for common anode or common cathode display

Feel free to fork the projects, just remember those are all released under the [Creative Commons BY-SA-NC](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.

If, instead, you are looking for the Gerbers only, have a look at the `pcb` folder in the sources:
* the `othermill` subfolder contains the `dip` (aka `model D`) and `smd` (aka `model F`) versions, both common anode and common cathode display, single or double layer
* the `model B` subfolder contains the,  now outdated, `3020` version in through hole package for common anode display only
* the `model C` subfolder contains the `3020` version in through hole package for common anode display only

Also, to determine the best resistor values, you can use a [Schmitt trigger simulation](http://tinyurl.com/y7sqfkgh) along with a [Schmitt trigger calculator](https://www.random-science-tools.com/electronics/inverting-schmitt-trigger-calculator.htm).
